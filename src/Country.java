public class Country{
    private String name;
    private String capital;
    private int area;
    private int population;
    private String currency;

    public Country(String name, String capital, int area, int population, String currency) {
        this.name = name;
        this.capital = capital;
        this.area = area;
        this.population = population;
        this.currency = currency;
    }

    public String name() { return name; }
    public String capital() { return capital; }
    public int area() { return area; }
    public int population() { return population; }
    public String currency() { return currency; }
    public int density(){ return population / area; }

    public String dottedArea(){ return dotter(area); }
    public String dottedPopulation(){ return dotter(population); }

    String dotter(int num){
        String tmp = "", res = "";
        int digits = 0;

        while(num != 0){
            if(digits % 3 == 0 && digits > 0)
                tmp += ".";

            tmp += (num % 10);
            num /= 10;
            digits++;
        }

        for (int i = tmp.length() - 1; i >= 0; i--) {
            res += tmp.charAt(i);
        }

        return res;
    }

    public String toString(){
        return name + ", " + capital + ", Area: " + dottedArea() + ", Population: " + dottedPopulation() + ", " + currency;
    }
}
