public class Test {

    static Country armenia = new Country("Armenia", "Yerevan", 29743, 2967900, "Dram");
    static Country austria = new Country("Austria", "Vienna", 83882, 8901064, "Euro");
    static Country belgium = new Country("Belgium", "Brussels", 30688, 11431406, "Euro");
    static Country czech_republic = new Country("Czech Republic", "Prague", 78866, 10610947, "Czech koruna");
    static Country cyprus = new Country("Cyprus", "Nicosia", 9251, 1170125, "Euro");
    static Country estonia = new Country("Estonia", "Tallinn", 45339, 1323824, "Euro");
    static Country montenegro = new Country("Montenegro", "Podgorica", 13812, 622000, "Euro");
    static Country poland = new Country("Poland", "Warsaw", 312685, 38422346, "Zloty");
    static Country slovakia = new Country("Slovakia", "Bratislava", 49035, 5435343, "Euro");
    static Country serbia = new Country("Serbia", "Belgrade", 88361, 7040272, "Serbian dinar");

    static Country[] countries = new Country[]{ estonia, czech_republic, armenia, belgium, cyprus, slovakia, poland,
        montenegro, serbia, austria};

    public static void main(String[] args) {
        //tree();
        list();
    }

    public static void tree(){
        CountryTree europeTree = new CountryTree("Europe Tree");
        System.out.println(europeTree.numberOfBodies());
        for (int i = 0; i < countries.length; i++) {
            europeTree.add(countries[i]);
        }
        /*
        europeTree.add(estonia);
        europeTree.add(czech_republic);
        europeTree.add(armenia);
        europeTree.add(belgium);
        europeTree.add(cyprus);
        europeTree.add(slovakia);
        europeTree.add(poland);
        europeTree.add(montenegro);
        europeTree.add(serbia);
        europeTree.add(austria);
         */

        System.out.println(europeTree.get("Slovakia"));
        System.out.println(europeTree.get("Austria"));
        System.out.println(europeTree.get("New Zealand"));

        System.out.println(europeTree);
        System.out.println(europeTree.maxDepth());

        System.out.println(europeTree.numberOfBodies());
    }

    public static void list(){
        CountryList europeList = new CountryList("Europe");
        for (int i = 0; i < countries.length - 3; i++) {
            europeList.add(countries[i]);
        }
        System.out.println(europeList);
        europeList.add(0, montenegro);
        System.out.println(europeList);

        CountryList rev = europeList.reverse();
        System.out.println(rev);

        rev.remove(2);  //Cyprus
        rev.remove(0);
        rev.remove(rev.size() - 1);
        rev.remove(69);

        System.out.println("Remove: \n" + rev);
    }
}